;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :render
  (:use :common-lisp)
  (:export :init-renderer))

(in-package :render)

(defparameter *vert-shader-src* "
#version 330 core

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

in vec2 position;
in vec2 in_texcoord;

out vec2 out_texcoord;

void main() {
	out_texcoord = in_texcoord;
	gl_Position = projection * view * model * vec4(position, 0.0f, 1.0f);
}
"
  "OpenGL vertex shader source code.")

(defparameter *frag-shader-src* "
#version 330 core

uniform sampler2D gltex;

in vec2 out_texcoord;

out vec4 outputColor;

void main() {
	vec4 tmp_color = texture(gltex, out_texcoord);
	if (tmp_color.a < 0.1) discard;
	outputColor = tmp_color;
}
"
  "OpenGL fragment shader source code.")

(defparameter *gl-vao* nil
  "OpenGL Vertex Attribute Object.")

(defparameter *gl-vbo* nil
  "OpenGL Vertex Buffer Object.")

(defparameter *gl-shader-program* nil
  "OpenGL shader program.")

(defparameter *gl-element-array* nil
  "OpenGL vertex element array.")

(defparameter *gl-projection-matrix*
  (matrix-utils:make-ortho-projection-matrix -400 400 -300 300 -1 1)
  "OpenGL projection matrix.")

(defparameter *gl-view-matrix* nil
  "OpenGL view matrix.")

(defparameter *basic-font* nil
  "Standard debugging font.")

(defmacro uniform-let (uniform-binds &body body)
  "Much like a LET, where each (VARIABLE UNIFORM) pair in UNIFORM-BINDS expands
  to a binding of the appropriate uniform from the current OpenGL shader
  program."
  `(let ,(loop for (name uniform) in uniform-binds collecting
              `(,name (gl:get-uniform-location *gl-shader-program* ,uniform)))
     ,@body))

(defmacro with-gl-vao (vao &body body)
  "Use a specific VAO within the OpenGL code in BODY."
  `(progn (gl:bind-vertex-array ,vao)
          (unwind-protect (progn ,@body)
            (gl:bind-vertex-array 0))))

(defun init-renderer ()
  "Initialize the renderer."
  (setf *basic-font* (sdl2-ttf:open-font #P"/usr/share/fonts/dejavu/DejaVuSansMono.ttf" 10))
  (let* ((glverts (gl-utils:float-vector->gl-array
		   #(-1.0  1.0 0.0 0.0    ;; Top left
		     -1.0 -1.0 0.0 1.0    ;; Bottom left
		      1.0 -1.0 1.0 1.0    ;; Bottom right
		      1.0  1.0 1.0 0.0))) ;; Top right
         (shader-program (gl-utils:build-shader-program
				(gl-utils:build-shader :vertex-shader *vert-shader-src*)
				(gl-utils:build-shader :fragment-shader *frag-shader-src*))))
    ;; Set the system-global OpenGL variables.
    (setf *gl-vao* (gl:gen-vertex-array))
    (setf *gl-vbo* (gl:gen-buffer))
    (setf *gl-shader-program* shader-program)
    (setf *gl-element-array* (gl-utils:ushort-vector->gl-array
                              #(0 1 2
                                2 3 0)))
    (setf *gl-position-attribute* (gl:get-attrib-location shader-program "position"))
    (setf *gl-texcoord-attribute* (gl:get-attrib-location shader-program "in_texcoord"))
    ;; Prep them for use.
    (gl:bind-buffer :array-buffer *gl-vbo*)
    (gl:buffer-data :array-buffer :static-draw glverts)
    (gl:free-gl-array glverts)
    (with-gl-vao *gl-vao*
      (gl:enable-vertex-attrib-array *gl-position-attribute*)
      (gl:vertex-attrib-pointer *gl-position-attribute* 2 :float :false
				(* 4 (cffi:foreign-type-size :float))
				0)
      (gl:enable-vertex-attrib-array *gl-texcoord-attribute*)
      (gl:vertex-attrib-pointer *gl-texcoord-attribute* 2 :float :false
				(* 4 (cffi:foreign-type-size :float))
				(* 2 (cffi:foreign-type-size :float))))
    ;; Good to go. Unbind VBO to prevent spurious rendering calls to mess things
    ;; up.
    (gl:bind-buffer :array-buffer 0))
  ;; Register to the message bus
  (message-bus:register-to-bus :render)
  (flet ((clear-handler (token kind body)
	   (declare (ignore token kind body))
	   (clear))
	 (set-view-matrix-handler (token kind body)
	   (declare (ignore token kind))
	   (set-view-matrix body))
	 (render-entity-handler (token kind body)
	   (declare (ignore token kind))
	   (render-entity body))
	 (render-overlay-handler (token kind body)
	   (declare (ignore token kind body))
	   (render-overlay)))
    (message-bus:register-callback :render :render-clear #'clear-handler)
    (message-bus:register-callback :render :render-set-view-matrix #'set-view-matrix-handler)
    (message-bus:register-callback :render :render-entity #'render-entity-handler)
    (message-bus:register-callback :render :render-overlay #'render-overlay-handler)))

(defun clear ()
  "Clear the screen."
  (gl:clear-color 0 0 0 1)
  (gl:clear :color-buffer-bit))

(defun set-view-matrix (matrix)
  "Set the OpenGL view matrix to MATRIX."
  (setf *gl-view-matrix* matrix))

(defun render-entity (entity)
  "Render ENTITY."
  (gl:use-program *gl-shader-program*)
  (with-gl-vao *gl-vao*
    (uniform-let ((projection "projection")
                  (view "view")
                  (model "model"))
      (gl:uniform-matrix-4fv projection *gl-projection-matrix* t)
      (gl:uniform-matrix-4fv view *gl-view-matrix*)
      (gl:uniform-matrix-4fv model (matrix-utils:make-entity-model-matrix entity))
      (gl:bind-texture :texture-2d (gl-utils:get-texture-by-name (entity:texture-id entity)))
      (gl:draw-elements :triangles *gl-element-array*))))

(defun render-text (x y align text &optional (color-red #xff) (color-green 0) (color-blue #xff))
  (let* ((text-surf-index (sdl2-ttf:render-text-solid *basic-font* text color-red color-green color-blue 0))
	 (text-surf (sdl2:convert-surface-format text-surf-index sdl2:+pixelformat-rgba8888+))
	 (text-tex-id (gl-utils:load-text-to-gl-texture text-surf))
	 (text-position-x (case align
			    (:center x)
			    ((:left :top-left :bottom-left) (+ x (/ (sdl2:surface-width text-surf) 2)))
			    ((:right :top-right :bottom-right) (- x (/ (sdl2:surface-width text-surf) 2)))
			    (otherwise 0)))
	 (text-position-y (case align
			    (:center y)
			    ((:top :top-left :top-right) (+ y (/ (sdl2:surface-height text-surf) 2)))
			    ((:bottom :bottom-left :bottom-right) (- y (/ (sdl2:surface-height text-surf) 2)))
			    (otherwise 0))))
    (gl:use-program *gl-shader-program*)
    (with-gl-vao *gl-vao*
      (uniform-let ((projection "projection")
                    (view "view")
                    (model "model"))
	(gl:uniform-matrix-4fv projection *gl-projection-matrix* t)
	(gl:uniform-matrix-4fv view *gl-view-matrix*)
	(gl:uniform-matrix-4fv model (matrix-utils:multiply-4x4-matrices
				      (matrix-utils:make-translation-matrix text-position-x text-position-y 0)
				      (matrix-utils:make-scale-matrix (/ (sdl2:surface-width text-surf) 2)
								      (- (sdl2:surface-height text-surf))
								      1)))
	(gl:bind-texture :texture-2d text-tex-id)
	(gl:draw-elements :triangles *gl-element-array*)
	(gl:delete-texture text-tex-id)))))

(defun render-overlay ()
  (render-text -390 -290 :top-left (format nil "~a" cl-user::*player*))
  (render-text 390 290 :bottom-right (format nil "FPS: ~d"
					     (handler-case
						 (round (/ 1000.0 (engine-time:get-engine-delta)))
					       (simple-error () 0)))))
