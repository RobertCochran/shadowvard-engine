;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :gl-utils
  (:use :common-lisp)
  (:export :float-vector->gl-array
           :ushort-vector->gl-array
           :build-shader
           :build-shader-program
           :load-img-to-gl-texture
	   :load-text-to-gl-texture
	   :reset-texture-list
	   :get-texture-by-name))

(in-package :gl-utils)

(defun vector->gl-array (vec vec-type gl-type)
  "Convert a vector VEC of type VEC-TYPE to an OpenGL array of GL-TYPE.

This function is internal to gl-utils. Use `float-vector->gl-array` or
`ushort-vector->gl-array` in external systems."
  (let ((glarr (gl:alloc-gl-array gl-type (length vec))))
    (dotimes (i (length vec))
      (setf (gl:glaref glarr i) (coerce (aref vec i) vec-type)))
    glarr))

(defun float-vector->gl-array (vec)
  "Convert a vector of single-floats VEC to an OpenGL array."
  (vector->gl-array vec 'single-float :float))

(defun ushort-vector->gl-array (vec)
  "Convert a vector of unsigned shorts VEC to an OpenGL array."
  (vector->gl-array vec '(integer 0 65535) :unsigned-short))

(defun build-shader (type src)
  "Compile a shader of TYPE from SRC.

When no errors occur, returns the compiled shader, otherwise signals an error."
  (let ((shader (gl:create-shader type)))
    (gl:shader-source shader src)
    (gl:compile-shader shader)
    (if (gl:get-shader shader :compile-status)
	shader
	(error "Shader compliation failed: ~a"
               (gl:get-shader-info-log shader)))))

(defun build-shader-program (&rest shaders)
  "Build a shader program from SHADERS.

When no errors occur, returns the linked shader program, otherwise signals an
error."
  (let ((shader-program (gl:create-program)))
    (dolist (shader shaders) (gl:attach-shader shader-program shader))
    (gl:bind-frag-data-location shader-program 0 "outputColor")
    (gl:link-program shader-program)
    (if (gl:get-program shader-program :link-status)
        shader-program
        (error "Shader program linking failed: ~a"
               (gl:get-program-info-log shader-program)))))

(defun load-img-to-gl-texture (path)
  "Load the image found in PATH and load it into an OpenGL texture."
  (let ((sdl-surf (sdl2-image:load-image path))
	(gltex (gl:gen-texture)))
    (gl:bind-texture :texture-2d gltex)
    (gl:tex-image-2d :texture-2d 0 :rgba
		     (sdl2:surface-width sdl-surf)
		     (sdl2:surface-height sdl-surf)
		     0 :rgba :unsigned-byte (sdl2:surface-pixels sdl-surf))
    (gl:generate-mipmap :texture-2d)
    (gl:tex-parameter :texture-2d :texture-min-filter :nearest-mipmap-nearest)
    (gl:tex-parameter :texture-2d :texture-mag-filter :nearest)
    (gl:tex-parameter :texture-2d :texture-wrap-s :repeat)
    (gl:tex-parameter :texture-2d :texture-wrap-t :repeat)
    (sdl2:free-surface sdl-surf)
    gltex))

(defun load-text-to-gl-texture (surf)
  "Load the text found in SURF and load it into an OpenGL texture."
  (let ((gltex (gl:gen-texture)))
    (gl:bind-texture :texture-2d gltex)
    (gl:tex-image-2d :texture-2d 0 :rgba
		     (sdl2:surface-width surf)
		     (sdl2:surface-height surf)
		     0 :rgba :unsigned-byte (sdl2:surface-pixels surf))
    (gl:generate-mipmap :texture-2d)
    (gl:tex-parameter :texture-2d :texture-min-filter :nearest-mipmap-nearest)
    (gl:tex-parameter :texture-2d :texture-mag-filter :nearest)
    (gl:tex-parameter :texture-2d :texture-wrap-s :repeat)
    (gl:tex-parameter :texture-2d :texture-wrap-t :repeat)
    ;(sdl2:free-surface surf)
    gltex))

(defparameter *texture-hash*
  (make-hash-table :test #'equal))

(defun reset-texture-list ()
  (setf *texture-hash* (make-hash-table :test #'equal)))

(defun get-texture-by-name (name)
  (multiple-value-bind (value presentp) (gethash name *texture-hash*)
    (if presentp
	value
	(setf (gethash name *texture-hash*)
	      (load-img-to-gl-texture (concatenate 'string name ".png"))))))
