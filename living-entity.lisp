;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :living-entity
  (:use :common-lisp
        :entity)
  (:export :living-entity
           :health
           :max-health
           :alivep))

(in-package :living-entity)

(defclass living-entity (entity)
  ((health
    :initarg :health
    :initform 100
    :accessor health)
   (max-health
    :initarg :max-health
    :initform 100
    :accessor max-health)))

(defgeneric alivep (entity)
  (:documentation "Non-nil when entity is alive."))

(defmethod alivep ((entity living-entity))
  (plusp (health entity)))

(defmethod alivep ((entity entity))
  ;;; Never alive, by definition
  nil)

(defmethod update ((entity living-entity) (delta number))
  (unless (plusp (health entity))
    (setf (active entity) nil))
  (call-next-method))
