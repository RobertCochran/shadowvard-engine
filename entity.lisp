;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :entity
  (:use :common-lisp)
  (:export :entity
           :active
	   :world
           :x
           :y
           :width
           :height
           :xvel
           :yvel
           :angle
	   :texture-id
           :print-object
           :collidep
           :update
           :on-collide))

(in-package :entity)

(defclass entity ()
  ((active
    :initarg :active
    :initform t
    :accessor active)
   (world
    :initarg :world
    :initform nil
    :accessor world)
   (x
    :initarg :x
    :initform 0
    :accessor x)
   (y
    :initarg :y
    :initform 0
    :accessor y)
   (width
    :initarg :width
    :initform 0
    :accessor width)
   (height
    :initarg :height
    :initform 0
    :accessor height)
   (xvel
    :initarg :xvel
    :initform 0
    :accessor xvel)
   (yvel
    :initarg :yvel
    :initform 0
    :accessor yvel)
   (angle
    :initarg :angle
    :initform 0
    :accessor angle)
   (texture-id
    :initarg :texture-id
    :initform -1
    :accessor texture-id)))

(defmethod print-object ((object entity) stream)
  (print-unreadable-object (object stream :type t)
    (with-slots (active x y width height xvel yvel angle) object
      (format stream
              ":active ~s :x ~s :y ~s :width ~s :height ~s :xvel ~s :yvel ~s :angle ~f"
              active x y width height xvel yvel angle))))

(defun gen-points (entity)
  "Make a list of points that comprise ENTITY's bounding box."
  (with-slots (x y width height angle) entity
    (let ((radian-angle (math-utils:degrees->radians (- angle 90))))
      (flet ((rotate-point (px py)
               (list (+ x (- (* (cos radian-angle) (- px x))
                             (* (sin radian-angle) (- py y))))
                     (+ y (+ (* (sin radian-angle) (- px x))
                             (* (cos radian-angle) (- py y)))))))
        (list (rotate-point (- x (/ width 2)) (- y (/ height 2)))
              (rotate-point (+ x (/ width 2)) (- y (/ height 2)))
              (rotate-point (+ x (/ width 2)) (+ y (/ height 2)))
              (rotate-point (- x (/ width 2)) (+ y (/ height 2))))))))

(defun gen-lines (entity)
  "Make a list of lines that comprise ENTITY's bounding box."
  (let ((points (gen-points entity)))
    (loop for i below (length points) collecting
         (list (nth i points) (nth (math-utils:wrap (1+ i) 0 4) points)))))

(defun collidep (entity-a entity-b)
  "Returns a Minimum Translation Vector list, ((cos sin) dist), such that moving
ENTITY-A by (* dist cos) on its X axis, and (* dist sin) on its Y axis, will
stop the collision when ENTITY-A and ENTITY-B collide, and nil when they do not
collide."
  ;;; Single Axis Theorem collision detection.
  (let* ((entity-a-points (gen-points entity-a))
         (entity-b-points (gen-points entity-b))
         ;; We need surfaces to project the shape vectors onto (in simple terms,
         ;; a place to cast the shadow of the shape). The normals of the lines
         ;; in the shapes will provide these surfaces.
         (normals
	  (mapcar #'math-utils:unitize-vector
		  (remove-duplicates
		   (nconc (mapcar #'math-utils:line-normal (gen-lines entity-a))
			  (mapcar #'math-utils:line-normal (gen-lines entity-b))))))
	 (last-minimum-overlap most-positive-fixnum)
	 (minimum-normal nil))
    (loop
       for normal in normals
       ;; Get the dot product of the normals and the points of the shapes. This
       ;; projects them (casts the shadow) so that we can compare the shadows.
       for a-dots = (mapcar (lambda (x) (math-utils:dot-product normal x))
                            entity-a-points)
       for b-dots = (mapcar (lambda (x) (math-utils:dot-product normal x))
                            entity-b-points)
       ;; Find the mininum and maximum positions of the shadows for easier
       ;; comparison. We only need the two bounding positions.
       for a-dot-min = (reduce #'min a-dots)
       for a-dot-max = (reduce #'max a-dots)
       for b-dot-min = (reduce #'min b-dots)
       for b-dot-max = (reduce #'max b-dots)
       ;; The Single Axis Theorem states that if there is a gap in the shadows
       ;; at *any* point, then the two shapes can't be colliding. If there is
       ;; never a gap, then the shapes must be colliding.
       never (or (< a-dot-max b-dot-min)
                 (< b-dot-max a-dot-min))
       ;; Keep track of collision information, so that we can return it if there
       ;; is a collision.
       minimize (if (< a-dot-max b-dot-min)
		    (- a-dot-max b-dot-min)
		    (- b-dot-max a-dot-min))
       into overlap
       if (< overlap last-minimum-overlap)
       do (setf minimum-normal normal
		last-minimum-overlap overlap)
       finally (return (list minimum-normal
			     overlap)))))

(defgeneric update (entity delta)
  (:documentation "Update ENTITY with a time interval of DELTA."))

(defmethod update ((entity entity) (delta number))
  (with-slots (x y xvel yvel angle) entity
    (setf angle (math-utils:wrap angle 0 360))
    (let ((radian-angle (math-utils:degrees->radians angle)))
      (incf x (* delta (+ (* yvel (cos radian-angle))
			  (* xvel (sin radian-angle)))))
      (incf y (* delta (+ (* yvel (sin radian-angle))
			  (* xvel (- (cos radian-angle)))))))))

(defgeneric on-collide (entity-a entity-b mtv)
  (:documentation "Method called when ENTITY-A collides with ENTITY-B."))
