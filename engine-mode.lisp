;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(ql:quickload "macros")

(defpackage :engine-mode
  (:use :common-lisp)
  (:export :engine-mode
	   :name
	   :tick-fun
	   :render-fun
	   :input-map
	   :current-engine-mode
	   :set-engine-mode))

(in-package :engine-mode)

(defparameter *engine-modes* nil)

(defun current-engine-mode ()
  *current-engine-mode*)

(defun set-engine-mode (new-mode)
  (macros:let-when (c (cleanup-fun *current-engine-mode*))
    (funcall c))
  (macros:let-when (s (setup-fun new-mode))
    (funcall s))
  (input-mapper:set-mapping (input-map new-mode))
  (setf *current-engine-mode* new-mode))

(defclass engine-mode ()
  ((name
    :initarg :name
    :initform (error "Name not given")
    :accessor name)
   (setup-fun
    :initarg :setup-fun
    :initform nil
    :accessor setup-fun)
   (cleanup-fun
    :initarg :cleanup-fun
    :initform nil
    :accessor cleanup-fun)
   (tick-fun
    :initarg :tick-fun
    :initform (error "Tick function not given")
    :accessor tick-fun)
   (render-fun
    :initarg :render-fun
    :initform (error "Render function not given")
    :accessor render-fun)
   (input-map
    :initarg :input-map
    :initform (error "Input mapping not given")
    :accessor input-map)))

(defparameter *current-engine-mode*
  (make-instance 'engine-mode:engine-mode
		 :name :dummy
		 :tick-fun (lambda ())
		 :render-fun (lambda ())
		 :input-map nil))
