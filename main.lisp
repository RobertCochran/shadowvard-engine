;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(ql:quickload "sdl2")
(ql:quickload "sdl2-image")
(ql:quickload "sdl2-ttf")
(ql:quickload "cl-opengl")

(ql:quickload "engine-time")
(ql:quickload "message-bus")
(ql:quickload "input-mapper")
(ql:quickload "matrix-utils")
(ql:quickload "gl-utils")
(ql:quickload "entity")
(ql:quickload "world")
(ql:quickload "living-entity")
(ql:quickload "player")
(ql:quickload "render")
(ql:quickload "world-entity")
(ql:quickload "engine-mode")

(defun handle-swank ()
  (when (find-package 'swank)
    (let ((connection (or swank::*emacs-connection*
			  (swank::default-connection))))
      (swank::handle-requests connection t))))

(defmacro with-main (&body body)
  `(sdl2:make-this-thread-main
    (lambda ()
      #+sbcl (sb-int:with-float-traps-masked (:invalid) ,@body)
      #-sbcl ,@body)))

(defparameter *player* nil)

(defun main ()
  (message-bus:init-bus)
  (input-mapper:init-mapper)
  (sdl2:with-init (:video)
    (sdl2-image:init '(:png))
    (sdl2-ttf:init)
    (sdl2:gl-set-attrs
     :context-profile-mask sdl2-ffi:+SDL-GL-CONTEXT-PROFILE-CORE+
     :context-major-version 3
     :context-minor-version 3)
    (sdl2:set-relative-mouse-mode 1)
    (unwind-protect
	 (sdl2:with-window (win :title "OGL" :flags '(:opengl))
	   (sdl2:gl-set-attr :doublebuffer 1)
	   (sdl2:with-gl-context (glc win)
	     (gl-utils:reset-texture-list)
	     (render:init-renderer)
	     (let* ((world (world:load-world-from-file #P"test-world.svw"))
		    (player (make-instance 'player:player
					   :width 40
					   :height 40
					   :angle 0
					   :texture-id "test-player")))
	       (setf *player* player)
	       (world:add-entity world player)
	       (engine-mode:set-engine-mode
		(make-instance 'engine-mode:engine-mode
			       :name :standard
			       :tick-fun
			       (lambda ()
				 (loop while (message-bus:queue-populated-p) do
				      (message-bus:bus-pump))
				 (world:update-entities world (engine-time:get-engine-delta)))
			       :render-fun
			       (lambda ()
				 (message-bus:bus-queue :main :render-clear nil)
				 (message-bus:bus-queue :main :render-set-view-matrix
							(matrix-utils:multiply-4x4-matrices
							 ;; TODO: X is somehow calculated
							 ;; backwards, figure out what's
							 ;; causing it...
							 (matrix-utils:make-scale-matrix
							  -1 1 1)
							 (matrix-utils:make-translation-matrix
							  0 (/ 400 3) 0)
							 (matrix-utils:make-rotation-matrix
							  (- 270 (entity:angle player)))
							 (matrix-utils:make-translation-matrix
							  (- (entity:x player))
							  (- (entity:y player))
							  0)))
				 (message-bus:bus-queue :main :render-entity world)
				 (loop for ent in (world:entity-list world) do
				      (message-bus:bus-queue :main :render-entity ent))
				 (message-bus:bus-queue :main :render-set-view-matrix
							(matrix-utils:multiply-4x4-matrices
							 (matrix-utils:make-translation-matrix 1 1 1)
							 (matrix-utils:make-scale-matrix 1 1 1)))
				 (message-bus:bus-queue :main :render-overlay nil))
			       :input-map
			       '(((:key-down :scancode-w) (:player-start-move :forward))
				 ((:key-down :scancode-s) (:player-start-move :back))
				 ((:key-down :scancode-a) (:player-start-move :left))
				 ((:key-down :scancode-d) (:player-start-move :right))
				 ((:key-up :scancode-w) (:player-stop-move :forward))
				 ((:key-up :scancode-s) (:player-stop-move :back))
				 ((:key-up :scancode-a) (:player-stop-move :left))
				 ((:key-up :scancode-d) (:player-stop-move :right))
				 ((:rel-mouse x y) (:player-rotate x y))
				 ((:mouse-down b) (:player-fire-weapon b)))))
	       (sdl2:with-event-loop (:method :poll)
		 (:idle ()
			(let ((current-mode (engine-mode:current-engine-mode)))
			  (engine-time:set-engine-delta)
			  (funcall (engine-mode:tick-fun current-mode))
			  (funcall (engine-mode:render-fun current-mode))
			  (sdl2:gl-swap-window win)
			  (handle-swank)))
		 (:keydown (:keysym keysym :repeat repeat)
			   (when (zerop repeat)
			     (message-bus:bus-queue :raw-input
						    :key-down
						    (sdl2:scancode keysym))))
		 (:keyup (:keysym keysym)
			 (message-bus:bus-queue :raw-input
						:key-up
						(sdl2:scancode keysym)))
		 (:mousemotion (:xrel xrel :yrel yrel :x x :y y)
			       (message-bus:bus-queue :raw-input
						      :rel-mouse
						      (list xrel yrel))
			       (message-bus:bus-queue :raw-input
						      :abs-mouse
						      (list x y)))
		 (:mousebuttondown (:button button)
				   (message-bus:bus-queue :raw-input
							  :mouse-down
							  (list button)))
		 (:mousebuttonup (:button button)
				   (message-bus:bus-queue :raw-input
							  :mouse-up
							  (list button)))
		 (:quit ()
			(sdl2-image:quit)
			t)))))
      (sdl2:set-relative-mouse-mode 0))))

(defun run-test ()
  (with-main (main)))
