;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :player
  (:use :common-lisp
	:entity
	:living-entity)
  (:export :player))

(in-package :player)

(defclass player (living-entity)
  ((bus-token
    :initarg :bus-token
    :initform (message-bus:register-to-bus (gensym "SV-PLAYER-")))))

(defmethod initialize-instance :after ((p player) &key)
  (with-slots (bus-token xvel yvel angle) p
    (flet ((update-player-start-move (token kind body)
	     (declare (ignore token kind))
	     (case (car body)
	       (:forward (incf yvel (/ 3)))
	       (:back (decf yvel (/ 3)))
	       (:left (decf xvel (/ 3)))
	       (:right (incf xvel (/ 3)))))
	   (update-player-stop-move (token kind body)
	     (declare (ignore token kind))
	     (case (car body)
	       (:forward (decf yvel (/ 3)))
	       (:back (incf yvel (/ 3)))
	       (:left (incf xvel (/ 3)))
	       (:right (decf xvel (/ 3)))))
	   (update-player-rotate (token kind body)
	     (declare (ignore token kind))
	     (destructuring-bind (xrel yrel) body
	       (declare (ignore yrel))
	       (decf angle (* (/ xrel 30) (engine-time:get-engine-delta)))))
	   (update-player-fire-weapon (token kind body)
	     (declare (ignore token kind body))
	     (world:add-entity (entity:world p)
				  (make-instance 'projectile:projectile
						 :x (entity:x p)
						 :y (entity:y p)
						 :width 10
						 :height 30
						 :yvel 3
						 :angle (entity:angle p)
						 :texture-id "projectile"))))
      (message-bus:register-callback bus-token :player-start-move #'update-player-start-move)
      (message-bus:register-callback bus-token :player-stop-move #'update-player-stop-move)
      (message-bus:register-callback bus-token :player-rotate #'update-player-rotate)
      (message-bus:register-callback bus-token :player-fire-weapon #'update-player-fire-weapon))))
