;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :matrix-utils
  (:use :common-lisp)
  (:export :make-ortho-projection-matrix
           :make-rotation-matrix
           :make-translation-matrix
           :make-scale-matrix
           :multiply-4x4-matrices
           :make-entity-model-matrix))

(in-package :matrix-utils)

(defun make-matrix-array (list)
  "Make LIST an array suitable for OpenGL uniform matrices.

This function is internal to matrix-utils. Use the transformation matrix
creation functions in external systems."
  (make-array
   16
   :element-type 'single-float
   :initial-contents
   (mapcar (lambda (x) (coerce x 'single-float)) list)))

(defun make-ortho-projection-matrix (left right top bottom near far)
  "Create an orthgraphic projection matrix such that the X coordinates range
from LEFT to RIGHT, Y coordinates range from TOP to BOTTOM, keeping all vertices
that have Z coordinates within NEAR to FAR."
  (make-matrix-array
   (list (/ 2.0 (- right left)) 0 0 (- (/ (+ right left) (- right left)))
         0 (/ 2.0 (- top bottom)) 0 (- (/ (+ top bottom) (- top bottom)))
         0 0 (/ -2.0 (- far near)) (- (/ (+ far near) (- far near)))
         0 0 0 1)))

(defun make-rotation-matrix (angle)
  "Create a rotation matrix that rotates by ANGLE, given in degrees."
  (let ((radian-angle (math-utils:degrees->radians angle)))
    (make-matrix-array
     (list (cos radian-angle) (- (sin radian-angle)) 0 0
           (sin radian-angle) (cos radian-angle) 0 0
           0 0 1 0
           0 0 0 1))))

(defun make-translation-matrix (x y z)
  "Create a translation matrix that translates by X, Y, and Z."
  (make-matrix-array
   (list 1 0 0 x
         0 1 0 y
         0 0 1 z
         0 0 0 1)))

(defun make-scale-matrix (x y z)
  "Create a scaling matrix that scales by X, Y, and Z."
  (make-matrix-array
   (list x 0 0 0
         0 y 0 0
         0 0 z 0
         0 0 0 1)))

(defun multiply-4x4-matrices (&rest matrices)
  "Multiply MATRICES from right to left."
  (flet ((multiply-4x4-matrix (matrix-1 matrix-2)
           "Multiply MATRIX-2 by MATRIX-1."
           (loop for col below 4 nconcing
                (loop for row below 4 collecting
                     (reduce #'+ (loop for i below 4 collecting
                                      (* (elt matrix-2 (+ (* i 4) row))
                                         (elt matrix-1 (+ (* col 4) i)))))))))
    (make-matrix-array (reduce #'multiply-4x4-matrix matrices :from-end t))))

(defun make-entity-model-matrix (entity)
  "Build an OpenGL model matrix for ENTITY."
  (multiply-4x4-matrices (make-translation-matrix
                          (entity:x entity)
                          (entity:y entity)
                          0)
                         (make-rotation-matrix
                          (- (entity:angle entity) 90))
                         (make-scale-matrix
                          (/ (entity:width entity) 2)
                          (/ (entity:height entity) 2)
                          0)))
