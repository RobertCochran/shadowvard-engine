;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :input-mapper
  (:use :common-lisp)
  (:export :init-mapper
	   :set-mapping))

(in-package :input-mapper)

(defparameter *current-input-map* nil)

(defun set-mapping (new-mapping)
  (setf *current-input-map* new-mapping))

(defun list-or-append (head tail)
  (if (listp tail)
      (append (if (listp head)
		  head
		  (list head))
	      tail)
      (list head tail)))

(defun get-mapping-for-event (kind &optional body)
  (pattern-translate:pattern-substitute
      (if body
	  (list-or-append kind body)
	  (list kind))
    *current-input-map*))

(defun mapping-event (mapping)
  (car mapping))

(defun mapping-body (mapping)
  (cdr mapping))

(defun do-mapping (token kind body)
  (declare (ignore token))
  (macros:let-when (input-binding (or (get-mapping-for-event kind body)
				      (get-mapping-for-event kind)))
    (message-bus:bus-queue :input-mapper
			   (mapping-event input-binding)
			   (mapping-body input-binding))))

(defun init-mapper ()
  (message-bus:register-to-bus :input-mapper)
  (message-bus:register-callback :input-mapper :key-down #'do-mapping)
  (message-bus:register-callback :input-mapper :key-up #'do-mapping)
  (message-bus:register-callback :input-mapper :mouse-down #'do-mapping)
  (message-bus:register-callback :input-mapper :rel-mouse #'do-mapping)
  (message-bus:register-callback :input-mapper :abs-mouse #'do-mapping))
