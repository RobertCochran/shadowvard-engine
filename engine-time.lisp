;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :engine-time
  (:use :common-lisp)
  (:export :engine-time
	   :get-engine-delta
	   :set-engine-delta))

(in-package :engine-time)

(defparameter *engine-time* 0)
(defparameter *engine-delta* 0)

(defun engine-time ()
  (sdl2:get-ticks))

(defun set-engine-delta ()
  (let ((now (engine-time)))
    (setf *engine-delta* (- now *engine-time*)
	  *engine-time* now)
    *engine-delta*))

(defun get-engine-delta ()
  *engine-delta*)
