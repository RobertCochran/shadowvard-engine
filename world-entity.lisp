;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :world-entity
  (:use :common-lisp)
  (:export :world-entity
           :print-object
           :update
           :on-collide))

(in-package :world-entity)

(defclass world-entity (entity:entity)
  ;; Hardcore xvel and yvel to be 0
  ((entity:xvel
    :initform 0)
   (entity:yvel
    :initform 0)))

(defmethod entity:update ((entity world-entity) (delta number))
  ;; Static object
  nil)

(defmethod entity:on-collide ((entity-a world-entity) (entity-b entity:entity) mtv)
  (destructuring-bind ((mtv-cos mtv-sin) mtv-dist) mtv
    (incf (entity:x entity-b) (* mtv-dist (- mtv-cos)))
    (incf (entity:y entity-b) (* mtv-dist (- mtv-sin)))))
