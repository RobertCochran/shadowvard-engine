;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :world
  (:use :common-lisp)
  (:export :world
	   :entity-list
	   :add-entity
           :remove-all-entities
           :update-entities
	   :load-world-from-file))

(in-package :world)

(defclass world (entity:entity)
  ((entity-list
    :initarg :entity-list
    :initform nil
    :accessor entity-list)
   (bounding-ents
    :initform nil
    :accessor bounding-ents)))

(defmethod initialize-instance :after ((w world) &key)
  (with-slots (bounding-ents
	       (x entity:x)
	       (y entity:y)
	       (width entity:width)
	       (height entity:height))
      w
    ; Left side
    (push (make-instance 'world-entity:world-entity
			 :width width
			 :height width
			 :x (- x width)
			 :y y)
	  bounding-ents)
    ; Right side
    (push (make-instance 'world-entity:world-entity
			 :width width
			 :height width
			 :x (+ x width)
			 :y y)
	  bounding-ents)
    ; Top side
    (push (make-instance 'world-entity:world-entity
			 :width height
			 :height height
			 :x x
			 :y (- y height))
	  bounding-ents)
    ; Bottom side
    (push (make-instance 'world-entity:world-entity
			 :width height
			 :height height
			 :x x
			 :y (+ y height))
	  bounding-ents)))

(defun add-entity (world entity)
  "Add ENTITY to the game world WORLD."
  (setf (entity:world entity) world)
  (push entity (entity-list world)))

(defun remove-all-entities (world)
  "Clear out the list of world entities.

This does *not* destroy the entities themselves; any outside references to them
will not be touched."
  (loop for e in (entity-list world) do
       (setf (entity:world e) nil))
  (setf (entity-list world) nil))

(defun entity-out-of-bounds-p (world entity)
  (with-slots ((world-x entity:x)
	       (world-y entity:y)
	       (world-w entity:width)
	       (world-h entity:height))
      world
    (and (or (< (entity:x entity) (- world-x (/ world-w 2)))
	     (> (entity:x entity) (+ world-x (/ world-w 2))))
	 (or (< (entity:y entity) (- world-y (/ world-h 2)))
	     (> (entity:y entity) (+ world-y (/ world-h 2)))))))

;; Dummy method for testing
(defmethod entity:on-collide ((entity-a entity:entity) (entity-b entity:entity) mtv)
  nil)

(defun update-entities (world delta)
  "Update all entities in the WORLD entity list.

This is a three-step operation. First, any entity whose ACTIVE slot is NIL will
be removed. Second, every entity will have its UPDATE method called. Lastly,
each entity is checked for collision. If there are any collisions, then both
entities will have their ON-COLLIDE method called with the other entity as a
second argument. That is, both (on-collide a b) and (on-collide b a) happens."
  (with-slots (entity-list bounding-ents) world
    (setf entity-list (remove-if-not #'entity:active entity-list))
    (dolist (entity entity-list)
      (entity:update entity delta))
    (setf entity-list (remove-if (lambda (e) (entity-out-of-bounds-p world e))
    				 entity-list))
    (let ((extended-entity-list (append entity-list bounding-ents)))
      (loop for (entity-a . rest-of-entities) on extended-entity-list do
	   (loop for entity-b in rest-of-entities do
		(macros:let-when (mtv (entity:collidep entity-a entity-b))
		  (entity:on-collide entity-a entity-b mtv)
		  (entity:on-collide entity-b entity-a (math-utils:invert-mtv-angle mtv))))))))

(defun load-world-from-stream (stream)
  (let* ((eof-sym (gensym))
	  new-world)
    (let ((first-form (read stream nil eof-sym)))
      (if (or (eq first-form eof-sym)
	      (null first-form))
	  (return-from load-world-from-stream 'empty)
	  ;; FIXME(?): Check first to see if the first form is the world
	  ;; definition proper. While in theory it should be possible to grovel
	  ;; through the definition lists and pick it out specifically, perhaps
	  ;; it should be mandated by format to list that one first...
	  (if (eq (first first-form) 'world:world)
	      (setf new-world (apply #'make-instance first-form))
	      (return-from load-world-from-stream 'malformed)))
      (loop for list = (read stream nil eof-sym)
	 while (not (eq list eof-sym))
	 do (add-entity new-world
			(apply #'make-instance list)))
      new-world)))

(defun load-world-from-file (filename)
  (with-open-file (stream filename :direction :input :if-does-not-exist :error)
    (load-world-from-stream stream)))
