;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :pattern-translate
  (:use :common-lisp)
  (:export :pattern-substitute))

(in-package :pattern-translate)

(defun pattern-placeholder-p (x)
  "Non-NIL if X would match as a variable in a pattern.

Currently this is non-keyword symbols."
  (and (symbolp x)
       (not (string= (package-name (symbol-package x)) "KEYWORD"))))

(defun pattern-matches-p (pattern expr)
  "Non-NIL if EXPR could match PATTERN."
  (and (= (length pattern) (length expr))
       (let (bind-list)
	 (loop
	    for expr-part in expr
	    for pattern-part in pattern
	    always (if (pattern-placeholder-p pattern-part)
		       (if (assoc pattern-part bind-list)
			   (equal (cdr (assoc pattern-part bind-list)) expr-part)
			   (push (cons pattern-part expr-part) bind-list))
		       (equal expr-part pattern-part))))))

(defun pattern-bind-list (pattern expr)
  "Create an alist of binding associations."
  (loop
     for expr-part in expr
     for pattern-part in pattern
     if (and (pattern-placeholder-p pattern-part)
	     (not (assoc pattern-part binds)))
     collect (cons pattern-part expr-part) into binds
     finally (return binds)))

(defun do-pattern-subst (pattern target expr)
  (reduce (lambda (sub res)
	    (subst (cdr sub) (car sub) res :test #'equal))
	  (pattern-bind-list pattern expr)
	  :initial-value target
	  :from-end t))

;; TODO: Document me... I'm already too complicated to not have any
;; documentation...
(defun pattern-substitute (expr clauses)
  (loop for (pattern target) in clauses
     if (pattern-matches-p pattern expr)
     return (do-pattern-subst pattern target expr)))
