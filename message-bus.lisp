;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2017 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :message-bus
  (:use :common-lisp)
  (:export :bus-message
	   :sender
	   :kind
	   :body
	   :print-object
	   :init-bus
	   :bus-queue
	   :register-to-bus
	   :deregister-from-bus
	   :register-callback
	   :queue-populated-p
	   :bus-pump))

(in-package :message-bus)

(defparameter *message-bus-queue* nil)

(defparameter *message-bus-listeners* nil)

(defclass bus-message ()
  ((sender :initarg :sender
	   :initform nil
	   :accessor sender)
   (kind :initarg :kind
	 :initform nil
	 :accessor kind)
   (body :initarg :body
	 :initform nil
	 :accessor body)))

(defmethod print-object ((msg bus-message) stream)
  (print-unreadable-object (msg stream :type t)
    (with-slots (sender kind body) msg
      (format stream ":sender ~s :kind ~s :body ~s"
	      sender kind body))))

(defun init-bus ()
  "Initialize message bus by resetting currently queued messages and listeners."
  (setf *message-bus-queue* (queues:make-queue :simple-queue))
  (setf *message-bus-listeners* nil)
  nil)

(defun bus-queue (sender kind body)
  "Add a message from SENDER, marked as a message of type KIND and with BODY as
extra information."
  (queues:qpush *message-bus-queue*
		(make-instance 'bus-message
			       :sender sender
			       :kind kind
			       :body body)))

(defun register-to-bus (&optional name)
  "Add NAME as a listener to message bus messages."
  ;;; TODO: Noodle finalizers so that stale objects can be GC'd
  ;;; See 'trivial-garbage'
  (let ((token (or name (gensym))))
    (push (list token nil)
	  *message-bus-listeners*)
    token))

(defun deregister-from-bus (token)
  "Remove NAME as a listener to message bus messages."
  (setf *message-bus-listeners*
	(remove token *message-bus-listeners* :key #'car)))

(defun register-callback (token kind fun)
  "Register a callback function FUN for message types KIND for listener TOKEN."
  (push (cons kind fun)
	(second (assoc token *message-bus-listeners*))))

(defun queue-populated-p ()
  "Non-NIL when there are messages in the queue."
  (plusp (queues:qsize *message-bus-queue*)))

(defun bus-pump ()
  "Handle a message bus message, ensuring it gets handled by registered listeners."
  (let ((pumped-msg (queues:qpop *message-bus-queue*)))
    (with-slots ((msg-sender sender)
		 (msg-kind kind)
		 (msg-body body))
	pumped-msg
      (let ((other-listeners (remove msg-sender
				     *message-bus-listeners*
				     :key #'car)))
	(dolist (listener other-listeners)
	  (destructuring-bind (token callbacks) listener
	    (declare (ignore token))
	    (macros:let-when (callback-cell (or (assoc msg-kind callbacks)
						(assoc :any callbacks)))
	      (funcall (cdr callback-cell) msg-sender msg-kind msg-body))))))))
