;;;; Shadowvard Engine - top-down 2D game engine
;;;; Copyright (C) 2016 Robert Cochran
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :math-utils
  (:use :common-lisp)
  (:export :degrees->radians
           :wrap
           :dot-product
           :line-normal
	   :unitize-vector
	   :invert-mtv-angle))

(in-package :math-utils)

(defun degrees->radians (angle)
  "Convert ANGLE from degrees to radians."
  (* angle (/ pi 180)))

(defun wrap (x low high)
  "Wrap X such that it is between LOW (inclusive) and HIGH (exclusive)."
  (let* ((range (- high low))
         (x (mod (- x low) range)))
    (+ x (if (minusp x)
             high
             low))))

(defun dot-product (point-a point-b)
  "Return the dot product of POINT-A and POINT-B."
  (+ (* (first point-a) (first point-b))
     (* (second point-a) (second point-b))))

(defun line-normal (line)
  "Returns a normal of LINE."
  (let ((point-a (first line))
        (point-b (second line)))
    (list (- (- (second point-b) (second point-a)))
          (- (first point-b) (first point-a)))))

(defun unitize-vector (vec)
  "Returns VECTOR normalized into a unit vector."
  (destructuring-bind (x y) vec
    (let ((vector-length (sqrt (+ (expt x 2)
				  (expt y 2)))))
      (list (/ x vector-length)
	    (/ y vector-length)))))

(defun invert-mtv-angle (mtv)
  "Return MTV with a 180 degree rotated angle"
  (destructuring-bind ((cos sin) dist) mtv
    (list (list (- cos) (- sin)) dist)))
